QOCIPlus: extension of Qt's OCI (Oracle) SQL driver
===================================================

The Qt's QSQL module (http://doc.qt.io/qt-5/qtsql-index.html) provides a number of drivers for commonly used database systems.
The functionality of these drivers focus on most commonly used features such as connection to a database, executing queries, use of bound parameters,
whereas it often neglects other features of interest for developers.

Notably, the QOCI driver for Oracle databases lacks 
* the possibility to gracefully abort the long-existing queriess, even though that the mechanism permits to do that (using the ocibreak());
  this is known as https://bugreports.qt.io/browse/QTBUG-4321
* clean shutdown of Oracle client libraries in multi-threaded Windows environment
* proper reaction on disconnection error codes reported by Oracle such as "not connected" or "connection lost"
  https://bugreports.qt.io/browse/QTBUG-10429
* has a limitation in the length of inserted string when bulk-insert is active (QT incident https://account.qt.io/support/request/INC-1153696)
* support for binary-double columns
* optimized fetch of blobs

The QOCIPlus driver is the extension of the original "QOCI" driver providing bugfixes and missing functionality.

 # Compiling
 To obtain source codes that are suitable to be compiled:
  * get the qt-5.9.2 source codes (from download.qt.io) and extract them
  * copy this patch inside the extracted qt source foder
  * apply the patch using the "-p1" flag:
  ```
     patch -p1 < QOCI.patch
  ```
  * proceed with compilation as instructed in the Qt's documentation

# Use

The driver that is compiled is a drop-in replacement for the original QOCI driver of Qt, and provides additional functionalities.

### Using the new cmdBreak() method
